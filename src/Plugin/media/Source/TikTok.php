<?php

namespace Drupal\media_entity_tiktok\Plugin\media\Source;

use Drupal\media\Plugin\media\Source\OEmbed;

/**
 * TikTok Media Entity Source.
 *
 * @MediaSource(
 *   id = "tiktok",
 *   label = @Translation("TikTok"),
 *   description = @Translation("Embed TikTok Video."),
 *   providers = {"TikTok"},
 *   allowed_field_types = {"string"},
 * )
 */
class TikTok extends OEmbed {
  // No need for anything in here;
  // the base plugin can take care of typical interactions
  // with external oEmbed services.
}
