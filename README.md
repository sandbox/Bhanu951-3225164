# Media Entity TikTok

Allows TikTok videos to be added as Media Entities.

Known Issues :

oEmbed system doesn't work if thumbnail url does not have a file extension

Core Issue ~[#3080666](https://www.drupal.org/project/drupal/issues/3080666)~

[#3225184](https://www.drupal.org/project/drupal/issues/3225184)

Sample URLs :

1. https://www.tiktok.com/@iffart__/video/6967719353416420610

2. https://www.tiktok.com/@xixilim/video/6963267968339938562

3. https://www.tiktok.com/@tutiandyuki/video/6966241107437047041

4. https://www.tiktok.com/@scout2015/video/6718335390845095173

5. https://www.tiktok.com/@tiagogreis/video/6830059644233223429

